## Инструкция по линтингу

`yarn lint` или `npm run lint` **ESLint** просмотрит все файлы в директории _src_ и выведет отчёт по файлам, в которых он нашёл ошибки.

`yarn lint:fix` или `npm run lint:fix` **ESLint** выполнит проверку и попытается исправить обнаруженные ошибки.

`yarn prettier` или `npm run prettier` **Prettier** приведет код к кодстайлу проекта.

`yarn format` или `npm run format` последовательно выполнит проверки - **Prettier** исправит стилистику, затем **ESLint** проведет проверку кода с иправлением ошибок.

**husky** перед каждым коммитом будет автоматически проводить проверку кода на ошибки. Если ошибки не обнаружатся, коммит успешно создастся, иначе коммит отменится и веведется отчет линтера об ошибках.

# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
  },
};
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
