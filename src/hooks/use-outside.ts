import { useEffect, useRef, useState } from 'react';

export const useOutside = (initialVisible: boolean) => {
  const [isShow, setIsShow] = useState<boolean>(initialVisible);
  const ref = useRef<HTMLDivElement | null>(null);

  const clickHendler = (e: MouseEvent) => {
    if (ref.current && !ref.current.contains(e.target as Node)) {
      setIsShow(false);
    }
  };

  useEffect(() => {
    document.body.addEventListener('click', clickHendler, true);
    return function () {
      document.body.removeEventListener('click', clickHendler, true);
    };
  });

  return { ref, isShow, setIsShow };
};
