import { AuthProvider } from '@/context/AuthProvider';
import { CanvasProvider } from './context/CanvasProvider';
import { Router } from './Router';

function App() {
  return (
    <AuthProvider>
      <CanvasProvider>
        <Router />
      </CanvasProvider>
    </AuthProvider>
  );
}

export default App;
