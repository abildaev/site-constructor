import { useState } from 'react';

export interface IValidationHook {
  isValid: boolean;
  validate: (value: string, val2?: string) => boolean;
}

export const useUsernameValidation = (): IValidationHook => {
  const [isNameValid, setValid] = useState<boolean>(false);
  const validateName = (username: string): boolean => {
    const r = username.trim() !== '' && username.length > 2;
    setValid(r);
    return isNameValid;
  };

  return {
    isValid: isNameValid,
    validate: validateName,
  };
};

export const useSurUsernameValidation = (): IValidationHook => {
  const [isSurNameValid, setValid] = useState<boolean>(false);
  const validateSurName = (username: string): boolean => {
    const r = username.trim() !== '' && username.length > 2;
    setValid(r);
    return isSurNameValid;
  };

  return {
    isValid: isSurNameValid,
    validate: validateSurName,
  };
};

export const usePasswordValidation = (): IValidationHook => {
  const [isPassValid, setValid] = useState<boolean>(false);
  const validatePass = (password: string): boolean => {
    const r = password.trim().length > 5;
    setValid(r);
    return isPassValid;
  };

  return {
    isValid: isPassValid,
    validate: validatePass,
  };
};

export const useConfirmPasswordValidation = (): IValidationHook => {
  const [isConfirmPassValid, setValid] = useState<boolean>(false);
  const validateConfirmPass = (password: string, firstPass?: string): boolean => {
    const r = password.trim().length > 5 && firstPass?.trim() === password.trim();
    setValid(r);
    return isConfirmPassValid;
  };

  return {
    isValid: isConfirmPassValid,
    validate: validateConfirmPass,
  };
};

export const useEmailValidation = (): IValidationHook => {
  const [isEmailValid, setValid] = useState<boolean>(false);
  const validateEmail = (email: string): boolean => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const r = emailRegex.test(email);
    setValid(r);
    return isEmailValid;
  };

  return {
    isValid: isEmailValid,
    validate: validateEmail,
  };
};
