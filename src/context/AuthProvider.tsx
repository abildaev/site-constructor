import { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  User as FirebaseUser,
  onAuthStateChanged,
  updateProfile,
  updateEmail,
} from 'firebase/auth';
import { auth } from '@/firebase';
import { FirebaseError } from 'firebase/app';
import { AuthContextType, User } from './type';

const AuthContext = createContext<AuthContextType>(null!);

export function AuthProvider({ children }: { children: ReactNode }) {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState<FirebaseUser | null>(() => {
    return auth.currentUser;
  });

  const signup = (newUser: User, callback: VoidFunction) => {
    setLoading(true);
    createUserWithEmailAndPassword(auth, newUser.email, newUser.password)
      .then((userCredential) => {
        void updateProfile(userCredential.user, {
          displayName: `${newUser.name} ${newUser.surname}`,
        });
        callback();
      })
      .catch((error: Error) => {
        if (error instanceof FirebaseError) {
          const errorCode = error.code;
          const errorMessage = error.message;
          if (errorCode === 'email-already-in-use') {
            alert('This email is already in use.');
          } else {
            alert(errorMessage);
          }
          console.log(error);
        }
      });
  };

  const signin = (newUser: User, callback: VoidFunction) => {
    setLoading(true);
    signInWithEmailAndPassword(auth, newUser.email, newUser.password)
      .then(() => {
        callback();
      })
      .catch((error: Error) => {
        if (error instanceof FirebaseError) {
          const errorCode = error.code;
          const errorMessage = error.message;
          if (errorCode === 'auth/user-not-found') {
            alert('No user corresponding to the given email.');
          } else if (errorCode === 'auth/wrong-password') {
            alert('Wrong password.');
          } else {
            alert(errorMessage);
          }
          console.log(error);
        }
      });
  };

  const signout = async (callback?: VoidFunction) => {
    setLoading(true);
    await signOut(auth);

    if (callback) {
      callback();
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const update = (updatedUser: Partial<User>, callback: VoidFunction) => {
    updateProfile(user!, {
      displayName: `${updatedUser.name} ${updatedUser.surname}`,
    })
      .then(() => {
        updateEmail(user!, updatedUser.email!)
          .then(() => {
            alert('редактирование прошло успешно');
          })
          .catch((error) => {
            alert(error);
          });
      })
      .catch((error) => {
        if (error instanceof FirebaseError) {
          const errorCode = error.code;
          const errorMessage = error.message;
          // eslint-disable-next-line @typescript-eslint/no-base-to-string, @typescript-eslint/restrict-template-expressions
          alert(`${errorCode} ${error} ${errorMessage}`);
        }
      });
  };

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
      setUser(currentUser);
      setLoading(false);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  const value = { user, signup, signin, signout, update, loading };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

// eslint-disable-next-line react-refresh/only-export-components
export function useAuth() {
  return useContext(AuthContext);
}
