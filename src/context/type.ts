import { User as FirebaseUser } from 'firebase/auth';

export interface User {
  email: string;
  password: string;
  name?: string;
  surname?: string;
}

export interface AuthContextType {
  user: FirebaseUser | null;
  signup: (user: User, callback: VoidFunction) => void;
  signin: (user: User, callback: VoidFunction) => void;
  signout: (callback?: VoidFunction) => void;
  update: (user: Partial<User>, callback: VoidFunction) => void;
  loading: boolean;
}

export interface ICanvasData {
  component?: string;
  id?: string;
  position?: { top: number; left: number };
  dimension?: { width: string; height: string };
  content?:
  | string
  | string[]
  | { text: string; href: string }
  | { type: string; value: string[] | string; checked?: boolean }[];
  type: string;
}

export interface ICanvasComponent {
  position?: { top: number; left: number };
  dimension?: { width: string; height: string };
  content?:
  | string
  | string[]
  | { text: string; href: string }
  | { type: string; value: string[] | string; checked?: boolean }[];
  id?: string;
  isReadOnly?: boolean;
}

export interface ICanvasComponentMain extends ICanvasComponent {
  type: string;
}

export interface ICanvasContext {
  state?: {
    activeSelection: Set<string>;
    enableQuillToolbar: boolean;
  };
  actions?: {
    setActiveSelection: React.Dispatch<React.SetStateAction<Set<string>>>;
    setEnableQuillToolbar: (state: boolean) => void;
  };
}
