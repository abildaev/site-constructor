import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import { deleteElement } from '@/store/site/componentsSlice';
import { ICanvasContext } from './type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { useAppSelector } from '@/hooks/use-app-selector';

export const CanvasContext = createContext<ICanvasContext>({});

export const CanvasProvider = ({ children }: { children: ReactNode }) => {
  const components = useAppSelector((state) => state.components);
  const dispatch = useAppDispatch();

  const [activeSelection, setActiveSelection] = useState<Set<string>>(new Set());
  const [enableQuillToolbar, setEnableQuillToolbar] = useState<boolean>(false);
  const isSelectAll = useRef<boolean>(false);

  const selectAllElement = useCallback(() => {
    isSelectAll.current = true;
    components.map((data) => activeSelection.add(data.id ?? ''));
    setActiveSelection(new Set(activeSelection));
  }, [activeSelection, components]);

  const handleKeyDown = useCallback(
    (event: KeyboardEvent) => {
      if (event.key === 'Delete') {
        dispatch(deleteElement([...activeSelection]));
        setActiveSelection(new Set());
      } else if (['a', 'A'].includes(event.key) && event.ctrlKey) {
        event.preventDefault();
        selectAllElement();
      }
    },
    [deleteElement, selectAllElement],
  );

  const outSideClickHandler = () => {
    isSelectAll.current = false;
    setActiveSelection(new Set());
  };

  const handleMouseDown = useCallback(() => {
    if (!isSelectAll.current) {
      return;
    }

    outSideClickHandler();
    isSelectAll.current = false;
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', handleKeyDown);
    document.addEventListener('mousedown', handleMouseDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
      document.removeEventListener('mousedown', handleMouseDown);
    };
  }, [handleKeyDown, handleMouseDown]);

  const context: ICanvasContext = {
    actions: {
      setActiveSelection,
      setEnableQuillToolbar,
    },
    state: {
      activeSelection,
      enableQuillToolbar,
    },
  };

  return <CanvasContext.Provider value={context}>{children}</CanvasContext.Provider>;
};

// eslint-disable-next-line react-refresh/only-export-components
export function useCanvas() {
  return useContext(CanvasContext);
}
