import { html } from '@components/SideBar/Download/utils/html';
import { Button } from 'react-bootstrap';

import { useAppSelector } from '@/hooks/use-app-selector';
import { useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';

export function PreviewPage() {
  const navigate = useNavigate();

  const previewRef = useRef<HTMLDivElement | null>(null);
  const { title, bgColor, canvasWidth, canvasHeight } = useAppSelector((state) => state.site);
  const components = useAppSelector((state) => state.components);

  useEffect(() => {
    const content = html(title, bgColor, components, canvasWidth, canvasHeight);

    if (previewRef.current) {
      previewRef.current.innerHTML = content;
    }
  }, []);

  return (
    <>
      <Button variant='warning' onClick={() => navigate(-1)}>
        назад
      </Button>
      <div ref={previewRef}></div>
    </>
  );
}
