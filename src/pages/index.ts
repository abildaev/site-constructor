import { MainPage } from '@pages/MainPage';
import { MePage } from '@pages/MePage';
import LoginPage from '@pages/auth/loginPage';
import SignupPage from '@pages/auth/signupPage';
import { NewSite } from '@/pages/NewSite';
import { SiteConstructor } from '@/pages/SiteConstructor';

export { MainPage, MePage, LoginPage, SignupPage, NewSite, SiteConstructor };
