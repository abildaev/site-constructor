/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { Filter, Search, SiteCard } from '@/components';
import { useAuth } from '@/context/AuthProvider';
import { db } from '@/firebase';
import { useAppSelector } from '@/hooks/use-app-selector';
import { collection, orderBy, query } from 'firebase/firestore';
import { Container, Row, Col } from 'react-bootstrap';
import { useCollection } from 'react-firebase-hooks/firestore';

export function MainPage() {
  const { user } = useAuth();

  const { type, direction } = useAppSelector((state) => state.sortMethod);
  const [values, loading, error] = useCollection(
    query(collection(db, 'users', user!.uid, 'projects'), orderBy(type, direction)),
  );

  const searchQuery = useAppSelector((state) => state.searchByTitle);

  const projects = values?.docs
    .map((doc) => {
      const data: any = doc.data();
      return { id: doc.id, title: data.canvas?.title, ...data };
    })
    .filter((project) => project.title.toLocaleLowerCase().includes(searchQuery));

  return (
    <Container className='pt-5 pb-5'>
      <Row>
        <Col lg={{ offset: 6, span: 6 }}>
          <div className='d-flex'>
            <Search />
            <Filter />
          </div>
        </Col>
      </Row>

      <h2 className='mb-4'>Список сайтов:</h2>
      {loading && 'LOADING'}
      {error && `ERROR: ${error.message}`}
      {projects && (
        <Row>
          {projects.map((el) => (
            <Col lg={4} key={el.id}>
              <SiteCard siteName={el.canvas?.title} siteID={el.id} />
            </Col>
          ))}
        </Row>
      )}
    </Container>
  );
}
