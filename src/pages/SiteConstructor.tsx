import { Buttons, CanvasContainer, Download, FormBlock, Save } from '@/components';
import { Col, Row } from 'react-bootstrap';
import { useAppSelector } from '@/hooks/use-app-selector';
import { PreviewLink } from '@/components/SideBar/PreviewLink/PreviewLink';

export function SiteConstructor() {
  const { title } = useAppSelector((state) => state.site);

  return (
    <Row style={{ marginLeft: 0, marginRight: 0 }}>
      <Col sm={2}>
        <FormBlock title={title} />
        <Buttons />
        <Save />
        <Download />
        <PreviewLink />
      </Col>
      <Col sm={10} style={{ paddingLeft: 0, paddingRight: 0 }}>
        <CanvasContainer />
      </Col>
    </Row>
  );
}
