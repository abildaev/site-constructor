import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '@/context/AuthProvider';
import { User } from '@/context/type';
import { Form, Button, Alert, Container, Row, Col } from 'react-bootstrap';
import {
  useConfirmPasswordValidation,
  useEmailValidation,
  usePasswordValidation,
  useUsernameValidation,
  useSurUsernameValidation,
} from '@/services/validations';

const RegistrationForm: React.FC = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [error, setError] = useState<string | null>(null);
  // TODO
  // setError('Ошибка при регистрации');
  const { user: currentUser, signup } = useAuth();
  const navigate = useNavigate();

  if (currentUser) {
    navigate('/');
  }

  const handleSignup = () => {
    const user: User = {
      email,
      password,
      name: firstName,
      surname: lastName,
    };

    signup(user, () => {
      navigate('/');
    });
  };

  const handleToLogin = () => {
    navigate('/login');
  };

  const { isValid: isNameValid, validate: validateName } = useUsernameValidation();
  const { isValid: isSurNameValid, validate: validateSurName } = useSurUsernameValidation();
  const { isValid: isPassValid, validate: validatePass } = usePasswordValidation();
  const { isValid: isConfirmPassValid, validate: validateConfirmPass } =
    useConfirmPasswordValidation();
  const { isValid: isEmailValid, validate: validateEmail } = useEmailValidation();

  const emailInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setEmail(val);
    validateEmail(val);
  };

  const passInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setPassword(val);
    validatePass(val);
  };
  const passConfirmInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setConfirmPassword(val);
    validateConfirmPass(val, password);
  };

  const nameInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setFirstName(val);
    validateName(val);
  };
  const lastNameInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setLastName(val);
    validateSurName(val);
  };

  return (
    <Container className='mt-5'>
      <Row className='justify-content-md-center'>
        <Col md='auto'>
          <Form className='p-5 border-1'>
            <Form.Group controlId='formBasicFirstName' className='mt-3'>
              <Form.Label>Имя</Form.Label>
              <Form.Control
                type='text'
                placeholder='Введите ваше имя'
                value={firstName}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => nameInput(e)}
              />
            </Form.Group>

            <Form.Group controlId='formBasicLastName' className='mt-3'>
              <Form.Label>Фамилия</Form.Label>
              <Form.Control
                type='text'
                placeholder='Введите вашу фамилию'
                value={lastName}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => lastNameInput(e)}
              />
            </Form.Group>

            <Form.Group controlId='formBasicEmail' className='mt-3'>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type='email'
                placeholder='Введите ваш email'
                value={email}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => emailInput(e)}
              />
            </Form.Group>

            <Form.Group controlId='formBasicPassword' className='mt-3'>
              <Form.Label>Пароль</Form.Label>
              <Form.Control
                type='password'
                placeholder='Введите ваш пароль'
                value={password}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => passInput(e)}
              />
            </Form.Group>

            <Form.Group controlId='formBasicConfirmPassword' className='mt-3'>
              <Form.Label>Повторите пароль</Form.Label>
              <Form.Control
                type='password'
                placeholder='Повторите ваш пароль'
                value={confirmPassword}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => passConfirmInput(e)}
              />
            </Form.Group>

            {error && <Alert variant='danger'>{error}</Alert>}

            <Button
              disabled={
                !isNameValid &&
                !isSurNameValid &&
                !isEmailValid &&
                !isPassValid &&
                !isConfirmPassValid
              }
              variant='primary'
              className='mt-3'
              onClick={handleSignup}
            >
              Зарегистрироваться
            </Button>

            <div className='mt-2'>
              <p>
                Уже есть аккаунт?{' '}
                <Button
                  className='text-decoration-none align-baseline'
                  variant='link'
                  onClick={handleToLogin}
                >
                  Войти
                </Button>
              </p>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default RegistrationForm;
