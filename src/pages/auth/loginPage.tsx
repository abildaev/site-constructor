import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '@/context/AuthProvider';
import { User } from '@/context/type';
import { Form, Button, Alert, Container, Row, Col } from 'react-bootstrap';
import { useEmailValidation, usePasswordValidation } from '@/services/validations';
const LoginPage: React.FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [error, setError] = useState<string | null>(null);
  // TODO
  // setError('Неверный email или пароль');
  const { user: currentUser, signin } = useAuth();
  const navigate = useNavigate();

  if (currentUser) {
    navigate('/');
  }
  const handleLogin = () => {
    const user: User = {
      email,
      password,
    };

    signin(user, () => {
      navigate('/');
    });
  };

  const handleToSignup = () => {
    navigate('/signup');
  };

  const { isValid: isPassValid, validate: validatePass } = usePasswordValidation();
  const { isValid: isEmailValid, validate: validateEmail } = useEmailValidation();

  const emailInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setEmail(val);
    validateEmail(val);
  };

  const passInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.currentTarget.value;
    setPassword(val);
    validatePass(val);
  };

  return (
    <Container className='mt-5'>
      <Row className='justify-content-md-center'>
        <Col md='auto'>
          <Form className='p-5 border-1'>
            <Form.Group className='mt-3' controlId='formBasicEmail'>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type='email'
                placeholder='Введите ваш email'
                value={email}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => emailInput(e)}
              />
            </Form.Group>

            <Form.Group className='mt-3' controlId='formBasicPassword'>
              <Form.Label>Пароль</Form.Label>
              <Form.Control
                type='password'
                placeholder='Введите ваш пароль'
                value={password}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => passInput(e)}
              />
            </Form.Group>

            {error && <Alert variant='danger'>{error}</Alert>}

            <Button
              variant='primary'
              disabled={!isEmailValid && !isPassValid}
              className='mt-3'
              onClick={handleLogin}
            >
              Войти
            </Button>

            <div className='mt-2'>
              <p>
                Нет аккаунта?{' '}
                <Button
                  className='text-decoration-none align-baseline'
                  variant='link'
                  onClick={handleToSignup}
                >
                  Зарегистрироваться
                </Button>
              </p>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default LoginPage;
