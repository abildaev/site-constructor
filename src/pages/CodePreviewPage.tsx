import { html } from '@components/SideBar/Download/utils/html';
import { useAppSelector } from '@/hooks/use-app-selector';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';

export function CodePreviewPage() {
  const navigate = useNavigate();

  const [code, setCode] = useState<string>('');
  const { title, bgColor, canvasWidth, canvasHeight } = useAppSelector((state) => state.site);
  const components = useAppSelector((state) => state.components);

  useEffect(() => {
    const content = html(title, bgColor, components, canvasWidth, canvasHeight);
    setCode(content);
  }, []);

  return (
    <div>
      <Button variant='warning' onClick={() => navigate(-1)}>
        назад
      </Button>
      <pre>
        <code>{code}</code>
      </pre>
    </div>
  );
}
