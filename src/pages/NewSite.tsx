import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Timestamp, addDoc, collection } from 'firebase/firestore';
import { useAuth } from '@/context/AuthProvider';
import { db } from '@/firebase';
import { deleteAllElement } from '@/store/site/componentsSlice';
import { setInitial } from '@/store/site/slice';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { useAppSelector } from '@/hooks/use-app-selector';

export function NewSite() {
  const { user } = useAuth();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  dispatch(deleteAllElement());
  dispatch(setInitial());

  const canvas = useAppSelector((state) => state.site);

  const addProject = async () => {
    try {
      const docRef = await addDoc(collection(db, 'users', user!.uid, 'projects'), {
        components: {},
        canvas,
        changed: Timestamp.now(),
      });
      navigate(`/sites/${docRef.id}`);
      console.log('Document written with ID: ', docRef.id);
    } catch (e) {
      console.error('Error adding document: ', e);
    }
  };

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    addProject();
  }, []);

  return <></>;
}
