import { useNavigate } from 'react-router-dom';
import { deleteDoc, doc, collection } from 'firebase/firestore';
import { Button, Card } from 'react-bootstrap';
import { useDocumentOnce } from 'react-firebase-hooks/firestore';
import { useAuth } from '@/context/AuthProvider';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { deleteAllElement, updateAllElement } from '@/store/site/componentsSlice';
import { IInitialState, setInitial, updateCanvas } from '@/store/site/slice';
import { ICanvasComponentMain } from '@/context/type';
import { db } from '@/firebase';

export const SiteCard = (props: { siteName: string; siteID: string }) => {
  const { user } = useAuth();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const [value] = useDocumentOnce(
    doc(collection(db, 'users', user!.uid, 'projects'), props.siteID),
  );
  const data = value?.data();

  const editProject = () => {
    dispatch(deleteAllElement());
    dispatch(setInitial());
    if (data) {
      dispatch(updateAllElement({ components: data.components as ICanvasComponentMain[] }));
      dispatch(updateCanvas({ canvas: data.canvas as IInitialState }));
    }
    navigate(`/sites/${props.siteID}`);
  };

  const deleteProject = async () => {
    await deleteDoc(doc(db, 'users', user!.uid, 'projects', props.siteID));
  };

  return (
    <Card className="mb-4">
      <Card.Body>
        <Card.Title>{props.siteName}</Card.Title>
      </Card.Body>
      <Card.Body>
        <Button variant='primary' className='me-2' onClick={editProject}>
          Редактировать
        </Button>
        <Button variant='danger' onClick={deleteProject}>
          Удалить
        </Button>
      </Card.Body>
    </Card>
  );
};
