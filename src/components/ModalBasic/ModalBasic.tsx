import { Dispatch, FC, ReactNode, SetStateAction } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

interface Props {
  show: boolean;
  setShow: Dispatch<SetStateAction<boolean>>;
  button: {
    text: string;
    clickHendler: () => void;
  };
  children: [ReactNode | string, ReactNode | string];
}

export const ModalBasic: FC<Props> = ({ show, setShow, button, children }) => {
  return (
    <Modal
      size='lg'
      aria-labelledby='contained-modal-title-vcenter'
      centered
      show={show}
      onHide={() => setShow(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title id='contained-modal-title-vcenter'>{children[0]}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{children[1]}</Modal.Body>
      <Modal.Footer>
        <Button onClick={() => setShow(false)}>Закрыть</Button>
        <Button onClick={button.clickHendler}>{button.text}</Button>
      </Modal.Footer>
    </Modal>
  );
};
