import { Button } from 'react-bootstrap';
import { html } from './utils/html';
import { useAppSelector } from '@/hooks/use-app-selector';

export const Download = () => {
  const { title, bgColor, canvasWidth, canvasHeight } = useAppSelector((state) => state.site);
  const components = useAppSelector((state) => state.components);

  const downloadFile = () => {
    const element = document.createElement('a');
    const file = new Blob([html(title, bgColor, components, canvasWidth, canvasHeight)], {
      type: 'text/html',
    });
    element.href = URL.createObjectURL(file);
    element.download = 'index.html';
    document.body.appendChild(element);
    element.click();
  };

  return (
    <>
      <Button
        href='#'
        variant='primary'
        onClick={downloadFile}
        style={{ marginTop: '10px', width: '100%' }}
      >
        Скачать сайт
      </Button>
    </>
  );
};
