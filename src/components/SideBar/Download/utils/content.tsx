/* eslint-disable @typescript-eslint/indent */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { ICanvasData } from '@/context/type';

export const content = (data: ICanvasData[]) => {
  return `
      ${data
        .map((item) => {
          if (item.type === 'LISTUL' || item.type === 'LISTOL') {
            return `
            <ul class="${item.id}">
               ${(item.content as string[]).map((el) => `<li>${el}</li>`).join(' ')}
            </ul>
            `;
          }

          if (item.type === 'TEXT') {
            return `<div class="${item.id}">${item.content}</div>`;
          }

          if (item.type === 'LINK') {
            return `<a class="${item.id}" href="${(item.content as any).href}">${(item.content as any).text}</a>`;
          }

          if (item.type === 'IMAGE') {
            return `<img class="${item.id}" src="${item.content}" />`;
          }

          if (item.type === 'FORM') {
            return `
         <form class="form ${item.id}">
            ${(item.content as { type: string; value: string[] | string; checked?: boolean }[])
              .map((el) => {
                switch (el.type) {
                  case 'input':
                    return `<input type="text" value="${el.value}" />`;
                  case 'textarea':
                    return `<textarea>${el.value}</textarea>`;
                  case 'select':
                    return `
               <select>
                  ${(el.value as string[]).map((sel) => `<option value="${sel}">${sel}</option>`)}
               </select>
               `;
                  case 'checkbox':
                    return `
                  <div>
                     <input type="checkbox" ${el.checked ? 'checked' : ''} />
                     <label>${el.value}</label>
                  </div>
               `;
                  case 'radio':
                    return `
                  <div>
                     <input type="radio" ${el.checked ? 'checked' : ''} />
                     <label>${el.value}</label>
                  </div>
               `;
                }
              })
              .join(' ')}
         <button type="submit">Отправить</button>
         </form>
         `;
          }
        })
        .join(' ')}
   `;
};
