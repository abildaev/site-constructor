import { ICanvasData } from '@/context/type';
import { content } from './content';
import { style } from './styles';

export const html = (
  title: string,
  bgColor: string,
  data: ICanvasData[],
  canvasWidth: number,
  canvasHeight: number,
) =>
  `
      <!doctype html>
      <html lang="en">
         <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>${title}</title>
            <style>${style(bgColor, data, canvasWidth, canvasHeight)}</style>
         </head>
         <body>
            <div id="root">
                
               <div class="container custom">
                    ${content(data)}
                </div>
               
            </div>
         </body>
      </html>
   `;
