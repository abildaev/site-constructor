import { ICanvasData } from '@/context/type';

export const style = (
  bgColor: string,
  data: ICanvasData[],
  canvasWidth: number,
  canvasHeight: number,
) =>
  `
   body{
      background-color: ${bgColor};
   }
   
   .custom {
      position: relative;
      margin: 0 auto;
      min-height: 100vh;
      height: ${canvasHeight}px;
      width: ${canvasWidth}px;
   }
   
   .form{
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      align-items: flex-start;
   }


   
   ${data
    .map((item) => {
      const addPx = item.dimension!.width.includes('px');
      return `
      .${item.id}{
         position: absolute;
         left: ${item.position!.left}px;
         top: ${item.position!.top}px;
         width: ${item.dimension!.width}${addPx ? '' : 'px'};
         height: ${item.dimension!.height}${addPx ? '' : 'px'};
      }
      `;
    })
    .join(' ')}
   `;
