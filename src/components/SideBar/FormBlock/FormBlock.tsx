import { Button, Form, InputGroup } from 'react-bootstrap';
import { Colorful } from '../Colorful';
import styles from './FormBlock.module.css';
import { useOutside } from '@/hooks/use-outside';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { changeCanvasHeight, handleTitle } from '@/store/site';
import { useAppSelector } from '@/hooks/use-app-selector';

interface FormBlockProps {
  title: string;
}

export function FormBlock({ title }: FormBlockProps) {
  const { bgColor, canvasHeight } = useAppSelector((state) => state.site);
  const { ref, isShow, setIsShow } = useOutside(false);
  const dispatch = useAppDispatch();

  return (
    <div className={styles.block}>
      <Form>
        <Form.Group className='mb-3' controlId='name'>
          <Form.Label>Название сайта</Form.Label>
          <Form.Control
            type='text'
            placeholder='Введите название'
            value={title}
            onChange={(e) => dispatch(handleTitle(e.target.value))}
          />
        </Form.Group>
        <Form.Group className='mb-3' controlId='name'>
          <Form.Label>Высота сайта (px)</Form.Label>
          <Form.Control
            type='number'
            placeholder='Введите высоту'
            value={canvasHeight}
            onChange={(e) => dispatch(changeCanvasHeight(+e.target.value))}
          />
        </Form.Group>
        <Form.Group className='mb-3' controlId='color' ref={ref}>
          <Form.Label>Выберите цвет</Form.Label>
          <InputGroup className='mb-2'>
            <Button style={{ width: '80%', cursor: 'pointer' }} onClick={() => setIsShow(!isShow)}>
              {bgColor || 'Выберите цвет'}
            </Button>
            <InputGroup.Text style={{ width: '20%' }}>&#10001;</InputGroup.Text>
          </InputGroup>
          <Colorful visible={isShow} />
        </Form.Group>
      </Form>
    </div>
  );
}
