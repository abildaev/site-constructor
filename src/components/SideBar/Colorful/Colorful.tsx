// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HexColorPicker } from 'react-colorful';
import styles from './Colorful.module.css';
import { changeBg } from '@/store/site';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { useAppSelector } from '@/hooks/use-app-selector';

interface Props {
  visible: boolean;
}

export const Colorful = ({ visible }: Props) => {
  const { bgColor } = useAppSelector((state) => state.site);
  const dispatch = useAppDispatch();

  return (
    <div className={visible ? `${styles.visible} ${styles.content}` : styles.content}>
      <HexColorPicker
        style={{ width: '100%', marginBottom: '15px' }}
        color={bgColor}
        onChange={(e) => dispatch(changeBg(e))}
      />
    </div>
  );
};
