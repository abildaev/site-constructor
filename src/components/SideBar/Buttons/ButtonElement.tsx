import { addElement } from '@/store/site/componentsSlice';
import { Button } from 'react-bootstrap';
import { ButtonType } from './type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';

interface Props {
  button: ButtonType;
}

export function ButtonElement({ button }: Props) {
  const dispatch = useAppDispatch();

  return (
    <Button
      variant='outline-secondary'
      style={{ width: '100%' }}
      onClick={() => dispatch(addElement(button.type))}
    >
      {button.img}
    </Button>
  );
}
