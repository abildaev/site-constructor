export interface ButtonType {
  id: number;
  img: JSX.Element;
  type: string;
}
