import { MdTitle } from 'react-icons/md';
import { FiExternalLink } from 'react-icons/fi';
import { FaListCheck, FaListOl, FaRegImages } from 'react-icons/fa6';
import { SiReacthookform } from 'react-icons/si';
import { ButtonType } from '../type';

export const buttonsBase: ButtonType[] = [
  { id: 1, img: <MdTitle />, type: 'TEXT' },
  { id: 2, img: <FiExternalLink />, type: 'LINK' },
  { id: 3, img: <FaListCheck />, type: 'LISTUL' },
  { id: 4, img: <FaListOl />, type: 'LISTOL' },
  { id: 5, img: <FaRegImages />, type: 'IMAGE' },
  { id: 6, img: <SiReacthookform />, type: 'FORM' },
];
