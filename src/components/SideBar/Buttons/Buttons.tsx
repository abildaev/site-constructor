import { Card, Col, Row } from 'react-bootstrap';
import { buttonsBase } from './const/base-buttons';
import { ButtonElement } from './ButtonElement';

export function Buttons() {
  return (
    <Card>
      <Card.Body style={{ padding: '5px 10px 5px 10px' }}>
        <Card.Text>Базовый</Card.Text>
        <Row>
          {buttonsBase.map((button) => (
            <Col sm={6} className='mb-2' key={button.id}>
              <ButtonElement button={button} />
            </Col>
          ))}
        </Row>
      </Card.Body>
    </Card>
  );
}
