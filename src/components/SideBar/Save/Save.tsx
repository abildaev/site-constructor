import { Button } from 'react-bootstrap';
import { useAppSelector } from '@/hooks/use-app-selector';
import { useAuth } from '@/context/AuthProvider';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { doc, setDoc, Timestamp } from 'firebase/firestore';
import { db } from '@/firebase';
import { useParams } from 'react-router-dom';

export const Save = () => {
  const components = useAppSelector((state) => state.components);
  const canvas = useAppSelector((state) => state.site);

  const { user } = useAuth();
  const { id } = useParams();

  const saveProject = async () => {
    try {
      const docRef = doc(db, 'users', user!.uid, 'projects', id!);
      await setDoc(docRef, { components, canvas, changed: Timestamp.now() });
      alert('Сохранено: ' + docRef.id);
    } catch (e) {
      // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
      alert(`Ошибка: ${e}`);
    }
  };

  return (
    <>
      <Button
        href='#'
        variant='primary'
        onClick={saveProject}
        style={{ marginTop: '10px', width: '100%' }}
      >
        Сохранить проект
      </Button>
    </>
  );
};
