import { Link } from 'react-router-dom';

export const PreviewLink = () => {
  return (
    <>
      <Link
        to='/sites/preview'
        className='d-block border-1 border-primary border text-decoration-none text-center py-2 rounded-2'
        style={{ marginTop: '10px', width: '100%' }}
      >
        Предпросмотр
      </Link>

      <Link
        to='/sites/codepreview'
        className='d-block border-1 border-primary border text-decoration-none text-center py-2 rounded-2'
        style={{ marginTop: '10px', width: '100%' }}
      >
        Посмотреть код сайта
      </Link>
    </>
  );
};
