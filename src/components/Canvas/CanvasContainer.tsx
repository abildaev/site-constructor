import { useContext, useEffect, useRef } from 'react';
import { CanvasContext } from '@/context/CanvasProvider';
import CanvasComponent from './components/CanvasComponent';
import Toolbar from './components/Toolbar';
import { changeCanvasWidth } from '@store/site';
import './styles.css';
import { useAppSelector } from '@/hooks/use-app-selector';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { useThrottle } from '@/hooks/useThrottle';

export const CanvasContainer = () => {
  const { state } = useContext(CanvasContext);
  const components = useAppSelector((s) => s.components);
  const canvas = useAppSelector((s) => s.site);
  const containerRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const throttledInputValue = useThrottle<number>(canvas?.canvasHeight, 500);

  useEffect(() => {
    if (containerRef.current) {
      const canvasWidth = containerRef.current.offsetWidth;
      dispatch(changeCanvasWidth(canvasWidth));
    }
  }, []);

  return (
    <div ref={containerRef}>
      <Toolbar isEditEnable={state!.enableQuillToolbar} />
      <div
        className='canvas-container'
        style={{ height: throttledInputValue, backgroundColor: canvas?.bgColor }}
      >
        {components.map((component) => (
          <CanvasComponent key={component.id} {...component} />
        ))}
      </div>
    </div>
  );
};
