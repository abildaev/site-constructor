export interface IToolbarProps {
  isEditEnable: boolean;
}

export interface IStyleAttributor {
  attrName: string;
  keyName: string;
  scope: number;
  whitelist: string[];
}
