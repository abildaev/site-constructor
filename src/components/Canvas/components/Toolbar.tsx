import { IToolbarProps } from './type';

/* eslint-disable react-refresh/only-export-components */
export const sizeList = [
  '8px',
  '9px',
  '10px',
  '11px',
  '12px',
  '14px',
  '16px',
  '18px',
  '20px',
  '72px',
];

export const fontList = [
  'Arial',
  'Arial Black',
  'Arial Unicode MS',
  'Calibri',
  'Cambria',
  'Cambria Math',
  'Candara',
  'Segoe UI, wf_segoe-ui_normal, helvetica, arial, sans-serif',
  'Comic Sans MS',
  'Consolas',
  'Constantia',
  'Corbel',
  'Courier New',
  'Georgia',
  'Lucida Sans Unicode',
  'Tahoma',
  'Times New Roman',
  'Trebuchet MS',
  'Verdana',
];

export default function Toolbar({ isEditEnable }: IToolbarProps) {
  return (
    <div style={{ display: 'flex' }}>
      {isEditEnable && (
        <div id='toolbar'>
          <select className='ql-font'>
            {fontList.map((font) => (
              <option key={font} value={font}>
                {font}
              </option>
            ))}
          </select>
          <select className='ql-size'>
            {sizeList.map((size) => (
              <option key={size} value={size}>
                {size}
              </option>
            ))}
          </select>
          <button className='ql-bold' />
          <button className='ql-italic' />
          <button className='ql-underline' />
          <select className='ql-align' />
          <select className='ql-color' />
          <select className='ql-background' />
        </div>
      )}
    </div>
  );
}
