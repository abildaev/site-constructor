/* eslint-disable @typescript-eslint/indent */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { useEffect, useState } from 'react';
import { Form, Row, Button, Col, InputGroup, FormControl } from 'react-bootstrap';
import { MdDelete, MdAdd } from 'react-icons/md';
import { buttonsForm } from './const';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { updateElementContent } from '@/store/site/componentsSlice';
import { ICanvasComponent } from '@/context/type';

export const FormContent = ({ id, dimension }: ICanvasComponent) => {
  const [formElements, setFormElements] = useState<
    { type: string; value: string[] | string; checked?: boolean }[]
  >([]);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(updateElementContent({ id, dimension, content: formElements }));
  }, [formElements]);

  const addFormElement = (element: string) => {
    const value = element === 'select' ? [] : '';
    const checked = false;
    setFormElements([
      ...formElements,
      element === 'checkbox' || element === 'radio'
        ? { type: element, value, checked }
        : { type: element, value },
    ]);
  };

  const updateFormElement = (
    index: number,
    value: string | string[] | boolean,
    field = 'value',
  ) => {
    setFormElements(
      formElements.map((item, idx) => (idx === index ? { ...item, [field]: value } : item)),
    );
  };

  const addSelectOption = (index: number) => {
    const newOption = prompt('Введите значение для новой опции');
    setFormElements(
      (formElements as any).map((item: any, idx: number) => {
        if (idx === index) {
          return { ...item, value: [...item.value, newOption] };
        }
        return item;
      }),
    );
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const deleteSelectOption = (formIndex: number, optionIndex: number) => {
    setFormElements(
      formElements.map((item, idx) => {
        if (idx === formIndex) {
          return {
            ...item,
            value: (item.value as any).filter((_: any, optIdx: number) => optIdx !== optionIndex),
          };
        }
        return item;
      }),
    );
  };

  const deleteFormElement = (index: number) => {
    setFormElements(formElements.filter((_, idx) => idx !== index));
  };

  return (
    <Form>
      <Row className='mb-2'>
        {buttonsForm.map((button) => (
          <Col key={button.id}>
            <Button
              variant='outline-secondary'
              style={{ width: '100%' }}
              onClick={() => addFormElement(button.type)}
              size='lg'
            >
              {button.img}
            </Button>
          </Col>
        ))}
      </Row>
      {formElements.map((element, index) => {
        switch (element.type) {
          case 'input':
            return (
              <div
                key={index}
                style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}
              >
                <Form.Control
                  type='text'
                  value={element.value}
                  onChange={(e) => updateFormElement(index, e.target.value)}
                />
                <MdDelete onClick={() => deleteFormElement(index)} />
              </div>
            );
          case 'select':
            return (
              <div
                key={index}
                style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}
              >
                {element.value.length ? (
                  <Form.Select>
                    {(element.value as string[]).map((option, optIndex) => (
                      <option key={optIndex}>{option}</option>
                    ))}
                  </Form.Select>
                ) : null}
                <MdAdd onClick={() => addSelectOption(index)} />
                <MdDelete onClick={() => deleteFormElement(index)} />
              </div>
            );
          case 'checkbox':
            return (
              <div
                key={index}
                className='form-element'
                style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}
              >
                <InputGroup className='mb-3'>
                  <InputGroup.Checkbox
                    aria-label='Checkbox for following text input'
                    checked={element.checked}
                    onChange={(e) => updateFormElement(index, e.target.checked, 'checked')}
                  />
                  <FormControl
                    aria-describedby='basic-addon1'
                    value={element.value}
                    onChange={(e) => updateFormElement(index, e.target.value, 'value')}
                  />
                </InputGroup>
                <MdDelete onClick={() => deleteFormElement(index)} />
              </div>
            );
          case 'radio':
            return (
              <div
                key={index}
                className='form-element'
                style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}
              >
                <InputGroup className='mb-3'>
                  <InputGroup.Radio
                    aria-label='Checkbox for following text input'
                    checked={element.checked}
                    onChange={(e) => updateFormElement(index, e.target.checked, 'checked')}
                  />
                  <FormControl
                    aria-describedby='basic-addon1'
                    value={element.value}
                    onChange={(e) => updateFormElement(index, e.target.value, 'value')}
                  />
                </InputGroup>
                <MdDelete onClick={() => deleteFormElement(index)} />
              </div>
            );
          case 'textarea':
            return (
              <div
                key={index}
                style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}
              >
                <Form.Control
                  as='textarea'
                  value={element.value}
                  onChange={(e) => updateFormElement(index, e.target.value)}
                />
                <MdDelete onClick={() => deleteFormElement(index)} />
              </div>
            );
          default:
            return null;
        }
      })}
    </Form>
  );
};
