/* eslint-disable @typescript-eslint/no-unused-vars */
import { Quill } from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { fontList, sizeList } from '../../Toolbar';
import { ICanvasComponent } from '@/context/type';
import { IStyleAttributor } from '../../type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { FormContent } from './FormContent';

const Size = Quill.import('attributors/style/size') as IStyleAttributor;
Size.whitelist = sizeList;

const Font = Quill.import('attributors/style/font') as IStyleAttributor;
Font.whitelist = fontList;

Quill.register(Font, true);
Quill.register(Size, true);

export const FormElement = ({ content, id, isReadOnly, dimension }: ICanvasComponent) => {
  return <FormContent id={id} dimension={dimension} />;
};
