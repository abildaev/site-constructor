import { RiInputCursorMove } from 'react-icons/ri';
import { BsTextareaResize } from 'react-icons/bs';
import { GoMultiSelect } from 'react-icons/go';
import { IoIosCheckboxOutline } from 'react-icons/io';
import { MdOutlineRadioButtonChecked } from 'react-icons/md';

export const buttonsForm = [
  { id: 1, img: <RiInputCursorMove />, type: 'input' },
  { id: 2, img: <BsTextareaResize />, type: 'textarea' },
  { id: 3, img: <GoMultiSelect />, type: 'select' },
  { id: 4, img: <IoIosCheckboxOutline />, type: 'checkbox' },
  { id: 5, img: <MdOutlineRadioButtonChecked />, type: 'radio' },
];
