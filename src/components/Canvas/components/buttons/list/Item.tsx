import { useRef } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import { IStyleAttributor } from '../../type';
import { fontList, sizeList } from '../../Toolbar';

interface Props {
  handleInputChange: (event: string, index: number) => void;
  isReadOnly: boolean | undefined;
  item: string;
  index: number;
}

const Size = Quill.import('attributors/style/size') as IStyleAttributor;
Size.whitelist = sizeList;

const Font = Quill.import('attributors/style/font') as IStyleAttributor;
Font.whitelist = fontList;

Quill.register(Font, true);
Quill.register(Size, true);

export const Item = ({ handleInputChange, isReadOnly, item, index }: Props) => {
  const ref = useRef(null);

  return (
    <>
      {isReadOnly ? (
        <div dangerouslySetInnerHTML={{ __html: item || 'Введите текст' }}></div>
      ) : (
        <ReactQuill
          ref={ref}
          readOnly={isReadOnly}
          theme='snow'
          className='quill-container'
          placeholder='Введите текст'
          value={item || undefined}
          modules={undefined}
          onChange={(event) => handleInputChange(event, index)}
        />
      )}
    </>
  );
};
