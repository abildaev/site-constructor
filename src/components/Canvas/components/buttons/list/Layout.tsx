import { ICanvasComponent } from '@/context/type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { updateElementContent } from '@/store/site/componentsSlice';
import { useEffect, useState } from 'react';
import { FaPlusCircle, FaTrash } from 'react-icons/fa';
import { Item } from './Item';

interface Props extends ICanvasComponent {
  Type: 'ul' | 'ol';
}

export const Layout = ({ Type, content, id, isReadOnly, dimension }: Props) => {
  const [items, setItems] = useState<string[]>(content as string[]);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(updateElementContent({ id, dimension, content: items }));
  }, [items]);

  const handleInputChange = (event: string, index: number) => {
    const newItems = [...items];
    newItems[index] = event;
    setItems(newItems);
  };

  const handleAddItem = () => {
    setItems([...items, '']);
  };

  const handleRemoveItem = (index: number) => {
    const newItems = [...items];
    newItems.splice(index, 1);
    setItems(newItems);
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: '10px 10px 0 0',
        background: '#fff',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
      }}
    >
      <Type>
        {items.map((item, index) => (
          <li key={index}>
            <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
              <Item
                handleInputChange={handleInputChange}
                isReadOnly={isReadOnly}
                item={item}
                index={index}
              />
              <FaTrash
                style={{ cursor: 'pointer', marginLeft: '5px' }}
                color='red'
                onClick={() => handleRemoveItem(index)}
              />
            </div>
          </li>
        ))}
      </Type>
      <FaPlusCircle
        style={{ cursor: 'pointer', width: 'auto', marginBottom: '5px' }}
        color='green'
        onClick={handleAddItem}
      />
    </div>
  );
};
