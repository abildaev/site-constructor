import { ICanvasComponent } from '@/context/type';
import { Layout } from './Layout';

export const ListOl = ({ content, id, isReadOnly, dimension }: ICanvasComponent) => {
  return (
    <Layout Type='ol' content={content} id={id} isReadOnly={isReadOnly} dimension={dimension} />
  );
};
