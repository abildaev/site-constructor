import { ICanvasComponent } from '@/context/type';
import { Layout } from './Layout';

export const ListUl = ({ content, id, isReadOnly, dimension }: ICanvasComponent) => {
  return (
    <Layout Type='ul' content={content} id={id} isReadOnly={isReadOnly} dimension={dimension} />
  );
};
