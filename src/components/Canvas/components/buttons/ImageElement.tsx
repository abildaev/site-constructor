import React, { useRef } from 'react';
import { updateElementContent } from '@/store/site/componentsSlice';
import { ICanvasComponent } from '@/context/type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { Button } from 'react-bootstrap';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const ImageElement = ({ content, id, isReadOnly, dimension }: ICanvasComponent) => {
  const dispatch = useAppDispatch();

  const uploadRef = useRef<HTMLInputElement>(null);

  const getBase64 = (file: File) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
      reader.readAsDataURL(file);
    });
  };

  const getImageDimensions = async (file: string): Promise<Record<string, number>> => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return new Promise((resolved, rejected) => {
      const i = new Image();
      i.onload = function () {
        resolved({
          w: i.width,
          h: i.height,
          nw: i.naturalWidth,
          nh: i.naturalHeight,
        });
      };
      i.src = file;
    });
  };

  const getAdjustedDimenstions = (width: number, height: number, resultWidth: number) => {
    const ratio = width / height;
    return { calcWidth: resultWidth, calcHeight: resultWidth / ratio };
  };

  const imageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e?.target?.files?.[0];
    if (file) {
      const base64 = (await getBase64(file)) as string;
      const imageDimensions: Record<string, number> = await getImageDimensions(base64);
      const { calcWidth, calcHeight } = getAdjustedDimenstions(
        imageDimensions?.nw,
        imageDimensions?.nh,
        150,
      );

      dispatch(
        updateElementContent({
          id,
          content: base64 || '',
          dimension: {
            width: `${calcWidth || 0}`,
            height: `${calcHeight || 0}`,
          },
        }),
      );
    }
  };

  const triggerUpload = () => {
    const element = uploadRef?.current;
    if (element) {
      element.click();
    }
  };

  const renderUploadContent = () => {
    return (
      <>
        <div className='image-upload-container' style={{ background: '#fff' }}>
          <Button style={{ cursor: 'pointer' }} onClick={triggerUpload}>
            Upload Image
          </Button>
        </div>
        <input
          ref={uploadRef}
          type='file'
          id='imageFile'
          name='imageFile'
          accept='.jpg, .png, .jpeg'
          onChange={imageUpload}
        />
      </>
    );
  };

  const renderImage = () => {
    return (
      <div
        style={{
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          backgroundImage: `url(${content})`,
          backgroundSize: 'contain',
          width: '100%',
          height: '100%',
          backgroundRepeat: 'no-repeat',
        }}
        className={id}
      />
    );
  };

  return <>{!content ? renderUploadContent() : renderImage()}</>;
};
