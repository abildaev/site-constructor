/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useRef, useState } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { updateElementContent } from '@/store/site/componentsSlice';
import { fontList, sizeList } from '../Toolbar';
import { ICanvasComponent } from '@/context/type';
import { IStyleAttributor } from '../type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { FiExternalLink } from 'react-icons/fi';

const Size = Quill.import('attributors/style/size') as IStyleAttributor;
Size.whitelist = sizeList;

const Font = Quill.import('attributors/style/font') as IStyleAttributor;
Font.whitelist = fontList;

Quill.register(Font, true);
Quill.register(Size, true);

export const LinkElement = ({ content, id, isReadOnly, dimension }: ICanvasComponent) => {
  const [visible, setVisible] = useState(false);
  const dispatch = useAppDispatch();
  const editorRef = useRef(null);
  const modules = { toolbar: '#toolbar' };

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
        {isReadOnly ? (
          <div dangerouslySetInnerHTML={{ __html: (content as any).text || 'Введите текст' }}></div>
        ) : (
          <ReactQuill
            ref={editorRef}
            readOnly={isReadOnly}
            theme='snow'
            className='quill-container'
            placeholder='Введите текст'
            modules={modules}
            value={(content as any).text}
            onChange={(e) =>
              dispatch(
                updateElementContent({ id, dimension, content: { ...(content as any), text: e } }),
              )
            }
          />
        )}
        <FiExternalLink
          onClick={() => setVisible(!visible)}
          style={{ cursor: 'pointer', margin: '0 5px', fontSize: '25px' }}
          color='red'
        />
        {visible ? (
          <textarea
            onChange={(e) =>
              dispatch(
                updateElementContent({
                  id,
                  dimension,
                  content: { ...(content as any), href: e.target.value },
                }),
              )
            }
            style={{ background: '#e9ecef' }}
            value={(content as any).href}
            placeholder='Введите ссылку'
          >
            https:
          </textarea>
        ) : null}
      </div>
    </>
  );
};
