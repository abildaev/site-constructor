// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { useRef } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { updateElementContent } from '@/store/site/componentsSlice';
import { fontList, sizeList } from '../Toolbar';
import { ICanvasComponent } from '@/context/type';
import { IStyleAttributor } from '../type';
import { useAppDispatch } from '@/hooks/use-app-dispatch';

const Size = Quill.import('attributors/style/size') as IStyleAttributor;
Size.whitelist = sizeList;

const Font = Quill.import('attributors/style/font') as IStyleAttributor;
Font.whitelist = fontList;

Quill.register(Font, true);
Quill.register(Size, true);

export const TextElement = ({ content, id, isReadOnly, dimension }: ICanvasComponent) => {
  const dispatch = useAppDispatch();
  const editorRef = useRef(null);

  const modules = {
    toolbar: '#toolbar',
  };

  return (
    <>
      {isReadOnly ? (
        <div dangerouslySetInnerHTML={{ __html: (content as string) || 'Введите текст' }}></div>
      ) : (
        <ReactQuill
          ref={editorRef}
          readOnly={isReadOnly}
          theme='snow'
          className='quill-container'
          placeholder='Введите текст'
          modules={modules}
          value={content as string}
          onChange={(e) => dispatch(updateElementContent({ id, content: e, dimension }))}
        />
      )}
    </>
  );
};
