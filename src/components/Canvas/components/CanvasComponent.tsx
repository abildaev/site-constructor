/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import React, { useContext, useRef, useState } from 'react';
import { ResizeEnable, Rnd } from 'react-rnd';
import { resizeHandleClasses } from '../utils/canvasUtils';
import { CanvasContext } from '@/context/CanvasProvider';
import { ICanvasComponentMain } from '@/context/type';
import { ImageElement, TextElement } from './index';
import { updateElementDimension, updateElementPosition } from '@/store/site/componentsSlice';
import { ListUl } from './buttons/list/ListUl';
import { ListOl } from './buttons/list/ListOl';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { LinkElement } from './buttons/LinkElement';
import { FormElement } from './buttons/form/FormElement';

const getEnableResize = (type: string): ResizeEnable => {
  return {
    bottom: type === 'IMAGE',
    bottomLeft: true,
    bottomRight: true,

    top: type === 'IMAGE',
    topLeft: true,
    topRight: true,

    left: true,
    right: true,
  };
};
const CanvasComponent = (props: ICanvasComponentMain) => {
  const { state, actions } = useContext(CanvasContext);
  const { dimension, position, content, id, type } = props;
  const [showGrids, setShowGrids] = useState(false);
  const [isReadOnly, setIsReadOnly] = useState(true);
  const isDragged = useRef<boolean>(false);

  const dispatch = useAppDispatch();

  const activeSelection = state?.activeSelection;

  const onBlur = (event: React.FocusEvent<HTMLDivElement>) => {
    const toolbarElement = document.querySelector('#toolbar');
    if (
      event.currentTarget.contains(event?.relatedTarget as Element) ||
      toolbarElement?.contains(event?.relatedTarget as Element)
    ) {
      return;
    }
    setIsReadOnly(true);
    actions?.setEnableQuillToolbar(false);
    if (id && activeSelection) {
      activeSelection.delete(id);
      actions?.setActiveSelection(new Set(activeSelection));
    }
  };

  const getComponent = () => {
    const componentMap = {
      TEXT: TextElement,
      IMAGE: ImageElement,
      LISTUL: ListUl,
      LISTOL: ListOl,
      LINK: LinkElement,
      FORM: FormElement,
    };

    const Component =
      componentMap[type as 'LINK' | 'TEXT' | 'IMAGE' | 'LISTUL' | 'LISTOL' | 'FORM'];
    return <Component id={id} isReadOnly={isReadOnly} content={content} dimension={dimension} />;
  };

  const style: React.CSSProperties = {
    outline: 'none',
    border: `2px solid ${(id && state?.activeSelection.has(id)) ?? showGrids ?? isDragged.current ? '#21DEE5' : 'transparent'}`,
  };

  const onMouseEnter = () => {
    setShowGrids(true);
  };

  const onMouseLeave = () => {
    setShowGrids(false);
  };

  const onfocus = () => {
    if (id) {
      actions?.setActiveSelection(new Set(state?.activeSelection.add(id)));
    }
  };

  const onKeyDown = (event: React.KeyboardEvent) => {
    if (!isReadOnly) event.stopPropagation();
  };

  const handleClass =
    id && state?.activeSelection.has(id) && state?.activeSelection.size === 1 ? 'showHandles' : '';

  const onDoubleClick = () => {
    if (!isReadOnly) return;
    setIsReadOnly(false);
    actions?.setEnableQuillToolbar(true);
  };

  return (
    <Rnd
      style={style}
      size={{ width: dimension?.width ?? 0, height: dimension?.height ?? 0 }}
      position={{ x: position?.left ?? 0, y: position?.top ?? 0 }}
      onDragStart={() => {
        isDragged.current = true;
      }}
      onDragStop={(e, d) => {
        isDragged.current = false;
        dispatch(updateElementPosition({ id, position: { left: d.x, top: d.y } }));
      }}
      resizeHandleWrapperClass={handleClass}
      resizeHandleClasses={resizeHandleClasses}
      onResize={(e, direction, ref, delta, pos) => {
        dispatch(
          updateElementDimension({
            id,
            dimension: { width: ref.style.width, height: ref.style.height },
            position: { top: pos.y, left: pos.x },
          }),
        );
      }}
      enableResizing={getEnableResize(type)}
      minWidth={100}
      minHeight={50}
      disableDragging={!isReadOnly}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onDoubleClick={onDoubleClick}
      onKeyDown={onKeyDown}
      onFocus={onfocus}
      onBlur={onBlur}
      tabIndex={0}
      lockAspectRatio={type === 'IMAGE'}
      bounds='parent'
    >
      <div className='item-container'>{getComponent()}</div>
    </Rnd>
  );
};

export default CanvasComponent;
