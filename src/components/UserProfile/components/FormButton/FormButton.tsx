import { Dispatch, FC, SetStateAction } from 'react';
import { Button } from 'react-bootstrap';
import { FaRegCircleCheck } from 'react-icons/fa6';
import { FaRegEdit } from 'react-icons/fa';
import { FieldErrors } from 'react-hook-form';
import { FormType } from '../../type';

interface Props {
  edit: boolean;
  setEdit: Dispatch<SetStateAction<boolean>>;
  errors: FieldErrors<FormType>;
}

export const FormButton: FC<Props> = ({ edit, setEdit, errors }) => {
  return (
    <Button
      type={edit ? 'submit' : 'button'}
      variant='link'
      style={{ position: 'absolute', top: 0, right: 0 }}
    >
      {edit ? (
        <FaRegCircleCheck
          size={30}
          style={{ color: Object.entries(errors).length === 0 ? '#228B22' : '#C0C0C0' }}
        />
      ) : (
        <FaRegEdit onClick={() => setEdit(true)} size={30} />
      )}
    </Button>
  );
};
