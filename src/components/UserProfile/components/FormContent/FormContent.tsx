import { FC } from 'react';
import { Card, Form } from 'react-bootstrap';
import { FieldErrors, UseFormRegister } from 'react-hook-form';
import { FormType } from '../../type';
import { paramsEmailInputs, paramsTextInputs } from './params-inputs';
import { useAuth } from '@/context/AuthProvider';

interface Props {
  edit: boolean;
  register: UseFormRegister<FormType>;
  errors: FieldErrors<FormType>;
}

export const FormContent: FC<Props> = ({ edit, register, errors }) => {
  const { user } = useAuth();

  return (
    <>
      {edit ? (
        <>
          <Form.Group className='mb-3'>
            <Form.Label>Имя</Form.Label>
            <Form.Control
              isInvalid={!!errors.firstname?.message}
              defaultValue={user?.displayName ? user.displayName.split(' ')[0] : ''}
              type='text'
              placeholder='Имя'
              {...paramsTextInputs('firstname', register)}
            />
            {errors.firstname?.message && (
              <Form.Control.Feedback type='invalid'>
                {errors.firstname.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Фамилия</Form.Label>
            <Form.Control
              isInvalid={!!errors.lastname?.message}
              defaultValue={user?.displayName ? user.displayName.split(' ')[1] : ''}
              type='text'
              placeholder='Фамилия'
              {...paramsTextInputs('lastname', register)}
            />
            {errors.lastname?.message && (
              <Form.Control.Feedback type='invalid'>
                {errors.lastname.message}
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Электронная почта</Form.Label>
            <Form.Control
              isInvalid={!!errors.email?.message}
              type='email'
              defaultValue={user?.email ? user.email : ''}
              placeholder='name@mail.ru'
              {...paramsEmailInputs(register)}
            />
            {errors.email?.message && (
              <Form.Control.Feedback type='invalid'>{errors.email.message}</Form.Control.Feedback>
            )}
          </Form.Group>
        </>
      ) : (
        <Card.Text className='text-center'>
          {user?.displayName}
          <br />
          {user?.email}
        </Card.Text>
      )}
    </>
  );
};
