import { UseFormRegister } from 'react-hook-form';
import { FormType } from '../../type';

type RegisterType = UseFormRegister<FormType>;
const required = 'Поле обязательно к заполнению';

export const paramsTextInputs = (name: 'firstname' | 'lastname', register: RegisterType) => {
  return {
    ...register(name, {
      required,
      pattern: {
        value: /^[а-яА-Яa-zA-Z0-9-]+$/,
        message: 'Разрешены только буквы и цифры',
      },
      minLength: { value: 2, message: `не менее ${2} символов` },
      maxLength: { value: 20, message: `не более ${20} символов` },
    }),
  };
};

export const paramsEmailInputs = (register: RegisterType) => {
  return {
    ...register('email', {
      required,
      pattern: {
        value: /\S+@\S+\.\S+/,
        message: 'Неверный формат email',
      },
    }),
  };
};
