export interface FormType {
  firstname: string;
  lastname: string;
  email: string;
}
