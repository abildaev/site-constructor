import { FC, useState } from 'react';
import { Card, Row, Col, Container, Form, Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { PiSmileyBlank } from 'react-icons/pi';
import { FormType } from './type';
import { FormContent } from './components/FormContent';
import { useAuth } from '@/context/AuthProvider';
import { FaRegCircleCheck } from 'react-icons/fa6';
import { FaRegEdit } from 'react-icons/fa';

export const UserProfile: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormType>();
  const [edit, setEdit] = useState(false);
  const { update, signout } = useAuth();

  const onSubmit = (data: FormType) => {
    const user = {
      email: data.email,
      name: data.firstname,
      surname: data.lastname,
    };
    update(user, () => null);
  };

  return (
    <Container>
      <Button
        className='align-baseline float-end link-dark'
        variant='link'
        onClick={() => signout()}
      >
        Выйти
      </Button>
      <Row className='justify-content-md-center'>
        <Col md='auto'>
          <Card className='mt-3'>
            <Card.Body>
              <Form noValidate>
                <Row className='justify-content-md-center'>
                  <PiSmileyBlank size={300} />
                </Row>
                <FormContent edit={edit} register={register} errors={errors} />
                {edit && (
                  <Button
                    type='button'
                    onClick={handleSubmit(onSubmit)}
                    variant='link'
                    style={{ position: 'absolute', top: 0, right: 0 }}
                  >
                    <FaRegCircleCheck
                      size={30}
                      style={{ color: Object.entries(errors).length === 0 ? '#228B22' : '#C0C0C0' }}
                    />
                  </Button>
                )}
                <Button
                  type='button'
                  variant='link'
                  onClick={() => setEdit(!edit)}
                  style={{ position: 'absolute', top: 0, right: edit ? '40px' : 0 }}
                >
                  <FaRegEdit size={30} />
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
