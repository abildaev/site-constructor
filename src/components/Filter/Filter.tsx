import Form from 'react-bootstrap/Form';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import { useState } from 'react';
import { Badge } from 'react-bootstrap';
import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { SortTypeMethod, changeSortingMethod } from '@/store/site/sortSlice';

export const Filter = () => {
  const [active, setActive] = useState<string | null>(null);

  const dispatch = useAppDispatch();

  return (
    <>
      <DropdownButton
        id='dropdown-button-dark-example2'
        variant='secondary'
        title='Сортировать по'
        onSelect={(key) => {
          dispatch(changeSortingMethod(key as SortTypeMethod));
          setActive(key);
        }}
      >
        <Dropdown.Item eventKey='a-z'>
          <Form.Check inline label='a-z' name='filter_group' type='radio' />
        </Dropdown.Item>
        <Dropdown.Item eventKey='z-a'>
          <Form.Check inline label='z-a' name='filter_group' type='radio' />
        </Dropdown.Item>
        <Dropdown.Item eventKey='new'>
          <Form.Check inline label='сначала новые' name='filter_group' type='radio' />
        </Dropdown.Item>
        <Dropdown.Item eventKey='old'>
          <Form.Check inline label='сначала старые' name='filter_group' type='radio' />
        </Dropdown.Item>
      </DropdownButton>
      <h3 className='mb-0'>
        <Badge className='ms-2' bg='secondary'>
          {active}
        </Badge>
      </h3>
    </>
  );
};
