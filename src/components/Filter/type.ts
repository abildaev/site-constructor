export interface DataType {
  name: string;
  date: Date;
}
