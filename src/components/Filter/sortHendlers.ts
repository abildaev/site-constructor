import { Dispatch, SetStateAction } from 'react';
import { DataType } from './type';

type SertHendlersType = Record<string, (setData: Dispatch<SetStateAction<DataType[]>>) => void>;

export const sertHendlers: SertHendlersType = {
  'a-z': (setData) => setData((prev) => prev.sort((a, b) => (a.name > b.name ? 1 : -1))),
  'z-a': (setData) => setData((prev) => prev.sort((a, b) => (a.name > b.name ? -1 : 1))),
  asc: (setData) =>
    setData((prev) => prev.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())),
  desc: (setData) =>
    setData((prev) => prev.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())),
};
