import { Dispatch, SetStateAction } from 'react';
import { sertHendlers } from './sortHendlers';
import { DataType } from './type';
import { NavigateFunction } from 'react-router-dom';

export const onSelect = (
  key: string | null,
  active: string | null,
  setActive: Dispatch<SetStateAction<string | null>>,
  setData: Dispatch<SetStateAction<DataType[]>>,
  navigate: NavigateFunction,
) => {
  if (active === key) {
    setActive(null);
    sertHendlers.asc(setData);
    navigate('/');
    localStorage.removeItem('sort-type');
  } else {
    setActive(key);
    // TODO
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    key && sertHendlers[key](setData);
    navigate(`?sort=${key}`);
    localStorage.setItem('sort-type', key!);
  }
};
