import { useAppDispatch } from '@/hooks/use-app-dispatch';
import { changeString, reset } from '@/store/site/findSlice';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

export const Search = () => {
  const [value, setValue] = useState('');
  const dispatch = useAppDispatch();

  const resetSearch = () => {
    dispatch(reset());
    setValue('');
  };

  return (
    <>
      <InputGroup className='me-2'>
        <Form.Control
          placeholder='Поиск'
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
        <Button variant='outline-secondary' onClick={() => dispatch(changeString(value))}>
          Поиск
        </Button>
        <Button variant='outline-secondary' onClick={resetSearch}>
          X
        </Button>
      </InputGroup>
    </>
  );
};
