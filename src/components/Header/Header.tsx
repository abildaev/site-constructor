import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';

export const Header = () => {
  return (
    <Navbar expand='lg' className='bg-body-tertiary'>
      <Container>
        <Navbar.Brand href='/'>Site-Consctructor</Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav className='me-auto'>
            <Link to='/' className={'me-2 link-dark link-underline-none'}>
              Главная
            </Link>
            <Link to='/sites/new' className={'me-2 link-dark'}>
              Новый сайт
            </Link>
          </Nav>
          <Nav className='justify-content-end'>
            <Link to='/me' className={'me-2 link-dark'}>
              Профиль
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
