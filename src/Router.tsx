import {
  createRoutesFromElements,
  createBrowserRouter,
  Route,
  RouterProvider,
} from 'react-router-dom';

import { MainPage, MePage, LoginPage, SignupPage, NewSite, SiteConstructor } from '@pages/index';
import { PrivateRoute } from '@hoc/privateRoutes';
import MainLayout from '@hoc/layouts/MainLayout';
import { PreviewPage } from '@pages/PreviewPage';
import { CodePreviewPage } from '@pages/CodePreviewPage';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route path='login' element={<LoginPage />} />
      <Route path='signup' element={<SignupPage />} />

      <Route path='/' element={<PrivateRoute />}>
        <Route path='/' element={<MainLayout />}>
          <Route index element={<MainPage />} />
          <Route path='me' element={<MePage />} />
          <Route path='sites'>
            <Route path='new' element={<NewSite />} />
            <Route path=':id' element={<SiteConstructor />} />
            <Route path='preview' element={<PreviewPage />} />
            <Route path='codepreview' element={<CodePreviewPage />} />
          </Route>
        </Route>
      </Route>
    </Route>,
  ),
);

export function Router() {
  return <RouterProvider router={router} />;
}
