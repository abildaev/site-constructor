import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyAz6K9QB0T7s7YY5GiSV1q3YbP3YUjkM-M',
  authDomain: 'site-constructor-5f9df.firebaseapp.com',
  projectId: 'site-constructor-5f9df',
  storageBucket: 'site-constructor-5f9df.appspot.com',
  messagingSenderId: '431118847196',
  appId: '1:431118847196:web:b16042c615d945829ab819',
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getFirestore(app);
