import { useAuth } from '@/context/AuthProvider';
import { useLocation, Outlet, Navigate } from 'react-router-dom';

function PrivateRoute() {
  const { user, loading } = useAuth();
  const location = useLocation();

  if (loading) {
    return <div>loading</div>;
  }

  return user ? (
    <div>
      <Outlet />
    </div>
  ) : (
    <Navigate to='/login' state={{ from: location }} replace />
  );
}

export default PrivateRoute;
