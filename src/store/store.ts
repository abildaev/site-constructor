import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { siteReducer } from '@store/site';
import { componentsSlice } from './site/componentsSlice';
import { sortSlice } from './site/sortSlice';
import { findSlice } from './site/findSlice';

const rootReducer = combineReducers({
  site: siteReducer,
  components: componentsSlice.reducer,
  sortMethod: sortSlice.reducer,
  searchByTitle: findSlice.reducer,
});

export const store = configureStore({
  reducer: rootReducer,
  devTools: true,
  // middleware: [],
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
