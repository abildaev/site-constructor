import { siteSlice } from '@store/site/slice';

export const siteReducer = siteSlice.reducer;

export const { handleTitle, changeBg, changeCanvasHeight, changeCanvasWidth } = siteSlice.actions;
