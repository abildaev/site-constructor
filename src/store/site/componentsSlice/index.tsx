/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { createSlice } from '@reduxjs/toolkit';
import { ICanvasComponentMain, ICanvasData } from '@/context/type';
import { getInitialData } from './utils';

export const componentsSlice = createSlice({
  name: 'components',
  initialState: [] as ICanvasComponentMain[],
  reducers: {
    addElement(state, { payload }: { payload: string }) {
      const defaultData = getInitialData(state, payload);
      state.push(defaultData as any);
    },
    deleteElement(state, { payload }: { payload: string[] }) {
      for (const id of payload) {
        state.splice(
          state.findIndex((component) => component.id === id),
          1,
        );
      }
    },
    deleteAllElement() {
      return [];
    },
    updateAllElement(state, { payload }: { payload: { components: ICanvasComponentMain[] } }) {
      if (payload && payload.components.length > 0) {
        state.push(...payload.components);
      }
    },
    updateElementPosition(state, { payload }: { payload: Partial<ICanvasData> }) {
      const component = state.find((comp) => comp.id === payload.id);
      if (component) {
        component.position = payload.position!;
      }
    },
    updateElementDimension(state, { payload }: { payload: Partial<ICanvasData> }) {
      const component = state.find((comp) => comp.id === payload.id);
      if (component) {
        component.dimension = payload.dimension!;
        component.position = payload.position!;
      }
    },
    updateElementContent(state, { payload }: { payload: Partial<ICanvasData> }) {
      const component = state.find((comp) => comp.id === payload.id);
      if (component) {
        component.dimension = payload.dimension!;
        component.content = payload.content!;
      }
    },
  },
});

export const {
  addElement,
  deleteElement,
  deleteAllElement,
  updateAllElement,
  updateElementPosition,
  updateElementDimension,
  updateElementContent,
} = componentsSlice.actions;
