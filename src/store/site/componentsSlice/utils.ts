import { ICanvasData } from '@/context/type';

export const getContent = (type: string) => {
  switch (type) {
    case 'LINK':
      return { text: '', href: '' };
    case 'TEXT':
      return '';
    case 'LISTUL':
      return ['', ''];
    case 'LISTOL':
      return ['', ''];
    case 'FORM':
      return [];
    default:
      return '';
  }
};

export const getInitialData = (data: ICanvasData[], type = 'TEXT') => {
  const id = `${type}__${Date.now()}__${data.length}`;

  return {
    type: type,
    id,
    position: {
      top: 100,
      left: 100,
    },
    dimension: {
      width: type === 'LISTUL' || type === 'LISTOL' ? '250' : type === 'FORM' ? '400' : '150',
      height: type === 'TEXT' || type === 'LINK' ? '60' : type === 'FORM' ? '200' : '150',
    },
    content: getContent(type),
  };
};
