import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface IInitialState {
  title: string;
  bgColor: string;
  canvasWidth: number;
  canvasHeight: number;
}

const initialState: IInitialState = {
  title: '',
  bgColor: '#cccccc',
  canvasWidth: 1024, /// default canvas width
  canvasHeight: 600,
};

export const siteSlice = createSlice({
  name: 'site',
  initialState,
  reducers: {
    handleTitle: (state, action: PayloadAction<string>) => {
      state.title = action.payload;
    },
    changeBg: (state, action: PayloadAction<string>) => {
      state.bgColor = action.payload;
    },

    changeCanvasWidth: (state, action: PayloadAction<number>) => {
      state.canvasWidth = action.payload;
    },
    changeCanvasHeight: (state, action: PayloadAction<number>) => {
      state.canvasHeight = action.payload;
    },
    setInitial: (state) => {
      state.title = initialState.title;
      state.bgColor = initialState.bgColor;
      state.canvasWidth = initialState.canvasWidth;
      state.canvasHeight = initialState.canvasHeight;
    },
    updateCanvas: (state, { payload }: { payload: { canvas: IInitialState } }) => {
      state.title = payload.canvas.title;
      state.bgColor = payload.canvas.bgColor;
      state.canvasWidth = payload.canvas.canvasWidth;
      state.canvasHeight = payload.canvas.canvasHeight;
    },
  },
});

export const { setInitial, updateCanvas } = siteSlice.actions;
