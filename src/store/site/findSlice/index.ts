import { createSlice } from '@reduxjs/toolkit';

export const findSlice = createSlice({
  name: 'searchByTitle',
  initialState: '',
  reducers: {
    changeString(state, { payload }: { payload: string }) {
      return (state = payload.toLocaleLowerCase());
    },
    reset() {
      return '';
    },
  },
});

export const { changeString, reset } = findSlice.actions;
