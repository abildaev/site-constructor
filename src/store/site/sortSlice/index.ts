import { createSlice } from '@reduxjs/toolkit';

export type SortTypeMethod = 'new' | 'old' | 'a-z' | 'z-a';

interface SortType {
  type: 'changed' | 'canvas.title';
  direction: undefined | 'desc';
}

export const sortSlice = createSlice({
  name: 'sortMethod',
  initialState: { type: 'changed', direction: undefined } satisfies SortType as SortType,
  reducers: {
    changeSortingMethod(state, { payload }: { payload: SortTypeMethod }) {
      switch (payload) {
        case 'new':
          return (state = { type: 'changed', direction: 'desc' });

        case 'old':
          return (state = { type: 'changed', direction: undefined });

        case 'a-z':
          return (state = { type: 'canvas.title', direction: undefined });

        case 'z-a':
          return (state = { type: 'canvas.title', direction: 'desc' });

        default:
          return state;
      }
    },
  },
});

export const { changeSortingMethod } = sortSlice.actions;
