import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      '@': '/src',
      '@components': '/src/components',
      '@pages': '/src/pages',
      '@types': '/src/@types',
      '@assets': '/src/assets',
      '@layouts': '/src/layouts',
      '@services': '/src/services',
      '@store': '/src/store',
      '@utils': '/src/utils',
      '@hoc': '/src/hoc'
    },
  },
});
